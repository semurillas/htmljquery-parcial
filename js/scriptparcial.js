$(function(){
    function createlist(results){
        var elem = $('#list-data');
        for(var index in results) {
            var firstName = results[index].name.first;
            var lastName = results[index].name.last;
            elem.append($('<li>').html(`${firstName} ${lastName}`))
        }
    }
    $('#btn-getinfo').click(() => {
        $.get('https://randomuser.me/api/?results=50&nat=es', 
            function(data){
                createlist(data.results);

            }, 'json');     
    });
});