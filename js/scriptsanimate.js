$(function () {
    function reanimate(selector, animation) {
        $(selector).removeClass(animation);
        setTimeout(() => {
            $(selector).addClass(animation);
        }, 10);


    }

    $('#btn-yes').click(() => {
        $('.box-main').append(
            $('<div>').addClass('box-content')
            .addClass('animate__animated')
            .addClass('animate__backInDown')
            .html('Contenido para mayores de edad. ')
            .append($('<br>'))
            .append($('<button>').addClass('btn-animated')
            .html('pulse para animar'))
        );
        $('.box-msg').remove();
        $('.btn-animated').click(() => {
             reanimate('.box-content', 'animate__backInDown');
        });
    });

    $('#btn-no').click(() => {
        $('.box-main').append(
            $('<div>').addClass('box-content')
            .addClass('animate__animated')
            .addClass('animate__backInDown')
            .html('NO eres apto para ingresar. ')
        );

        $('.box-msg').remove();
    });
});